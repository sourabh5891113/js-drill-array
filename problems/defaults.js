function defaults(obj, defaultProps) {
  if (typeof obj === "object" && typeof defaultProps === "object") {
    for (let key in defaultProps) {
      if (obj[key] === undefined) {
        obj[key] = defaultProps[key];
      }
    }
    return obj;
  } else {
    console.log("First and second argument should be an object");
    return {};
  }
}

module.exports = defaults;
