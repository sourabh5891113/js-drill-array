function values(obj) {
  if (typeof obj === "object") {
    let valuesArray = [];
    for (let key in obj) {
      if (typeof obj[key] !== "function") {
        valuesArray.push(obj[key]);
      }
    }
    return valuesArray;
  } else {
    console.log("First argument should be an object");
    return [];
  }
}

module.exports = values;
