function invert(obj) {
  if (typeof obj === "object") {
    let invertedObject = {};
    for (let key in obj) {
      invertedObject = { ...invertedObject, [obj[key]]: key };
    }
    return invertedObject;
  } else {
    console.log("First argument should be an object");
    return {};
  }
}

module.exports = invert;
