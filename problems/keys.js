function keys(obj) {
  if (typeof obj === "object") {
    let keysArray = [];
    for (let key in obj) {
      keysArray.push(key);
    }
    return keysArray;
  } else {
    console.log("First argument should be an object");
    return [];
  }
}

module.exports = keys;
