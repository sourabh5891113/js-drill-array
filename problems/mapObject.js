function mapObject(obj, cb) {
  if (typeof obj === "object") {
    let newObject = {};
    for (let key in obj) {
      newObject = { ...newObject, [key]: cb(obj[key]) };
    }
    return newObject;
  } else {
    console.log("First argument should be an object");
    return {};
  }
}
function cb(val) {
  return typeof val === "number" ? (val += 5) : val + " bond";
}

module.exports = { mapObject, cb };
