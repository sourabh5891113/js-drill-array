function pairs(obj) {
  if (typeof obj === "object") {
    let pairArray = [];
    for (let key in obj) {
      pairArray.push([key, obj[key]]);
    }
    return pairArray;
  } else {
    console.log("First argument should be an object");
    return [];
  }
}

module.exports = pairs;
